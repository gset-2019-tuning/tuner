import numpy as np
import alsaaudio as alsa
from scipy import signal
import sys
import time
import os

SAMPLERT = 48000
PERIOD = 2**10
BUF_SZ = 16

def get_buffer(iface):
    cnt = 0
    data = b''
    while True:
        length, _data = iface.read()
        if length != 0:
            data += _data
            cnt += 1
            if cnt >= BUF_SZ:
                return data

if __name__ == '__main__':
    iface = alsa.PCM(alsa.PCM_CAPTURE, alsa.PCM_NORMAL, sys.argv[1])
    iface.setchannels(1)
    iface.setrate(SAMPLERT)
    iface.setformat(alsa.PCM_FORMAT_S16_LE)
    iface.setperiodsize(PERIOD)

    print('waiting')
    for _ in range(5):
        get_buffer(iface)

    print('sampling')
    noise_samples = np.zeros((5, len(np.fft.rfftfreq(PERIOD * BUF_SZ))))
    for i in range(noise_samples.shape[0]):
        sig = np.frombuffer(get_buffer(iface), dtype='<i2') / 2**15
        windowed = sig * signal.hann(len(sig))
        f = np.abs(np.fft.rfft(windowed)) / len(sig) * 2
        f_d = 20 * np.log10(f)
        noise_samples[i,:] = f_d
    print('averaging')
    noise_sample = 10 ** (np.mean(noise_samples, axis=0) / 20)
    print(noise_sample.shape)
    print('saving')
    np.save('noise_sample.npy', noise_sample)
    print('done')
    os.system(f'kill -KILL {os.getpid()}')
