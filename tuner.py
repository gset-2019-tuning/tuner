import numpy as np
import alsaaudio as alsa
import math
import threading
import queue
import time
from scipy import signal
import operator
import functools

import frequency_estimator
from parabolic import parabolic


def _find_nearest(l, v):
    return sorted(l, key=lambda x: abs(x - v))[0]

def _find_nearest_i(l, v):
    return sorted(range(len(l)), key=lambda i: abs(l[i] - v))[0]

def _to_cents(f1, f2):
    return 1200 * math.log(f2 / f1, 2)


class Tuner:
    SAMPLERT = 48000
    PERIOD = 2**10
    BUF_SZ = 16
    PLOT = True

    LOW_CUT = 40

    TARGET_FUNDAMENTALS = [f*1 for f in [82.41, 110, 146.83, 196, 246.94, 329.63]]
    TARGET_NOTE_NAMES = ['E2', 'A2', 'D3', 'G3', 'B3', 'E4']
    FREQ_MARGIN_CENTS = 200
    N_OVERTONES = 2
    IN_TUNE_THRESH = 15 # cents
    INTENSITY_THRESH = 24 # dB below peak
    OVERTONE_IGNORE_THRESH = 5 # cents

    SNR_WINDOW = (100, 500)
    SNR_THRESH = 42

    def __init__(self, writer, card='default'):
        self.iface = alsa.PCM(alsa.PCM_CAPTURE, alsa.PCM_NORMAL, card)
        self.iface.setchannels(1)
        self.iface.setrate(self.SAMPLERT)
        self.iface.setformat(alsa.PCM_FORMAT_S16_LE)
        self.iface.setperiodsize(self.PERIOD)
        self.data=queue.Queue()
        self._data=b''

        targets = functools.reduce(operator.concat, [[f * n for n in range(1, self.N_OVERTONES + 2)] for f in sorted(self.TARGET_FUNDAMENTALS)], [])
        self.TARGET_FREQS = []
        for target in targets:
            if len(self.TARGET_FREQS) == 0:
                self.TARGET_FREQS.append(target)
                continue
            i = np.searchsorted(self.TARGET_FREQS, target)
            if i == len(self.TARGET_FREQS):
                i -= 1
            while i > 0 and abs(_to_cents(target, self.TARGET_FREQS[i])) <= self.OVERTONE_IGNORE_THRESH:
                self.TARGET_FREQS.remove(self.TARGET_FREQS[i])
                i -= 1
            i += 1
            while i < len(self.TARGET_FREQS) and abs(_to_cents(target, self.TARGET_FREQS[i])) <= self.OVERTONE_IGNORE_THRESH:
                self.TARGET_FREQS.remove(self.TARGET_FREQS[i])
                i += 1
            self.TARGET_FREQS.insert(i, target)

        self.writer = writer

        print(self.TARGET_FREQS)

        if self.PLOT:
            self.plt = __import__('matplotlib.pyplot').pyplot

        try:
            self.noise_sample = np.load('noise_sample.npy')
        except:
            self.noise_sample = None

    def freqs_from_fft(self, sig, fs):
        """
        Estimate frequency from peak of FFT
        """
        # Compute Fourier transform of windowed signal
        sig = sig / 2**15
        windowed = sig * signal.hann(len(sig))
        f = np.abs(np.fft.rfft(windowed)) / len(sig) * 2
        if self.noise_sample is not None:
            f = np.maximum(f - self.noise_sample, np.full(f.shape, 10**-10, dtype=f.dtype))
        f_d = 20*np.log10(f)

        to_freq = lambda i: fs * i / len(windowed)
        from_freq = lambda f: int(f / fs * len(windowed))

        snr_roi = f_d[from_freq(self.SNR_WINDOW[0]):from_freq(self.SNR_WINDOW[1])]
        snr = np.ptp(snr_roi)
        if snr < self.SNR_THRESH:
            return []

        if self.PLOT:
            plt_f = f_d[:from_freq(15000)]
            self.plt.plot([to_freq(i) for i in range(len(plt_f))], plt_f)
            self.plt.xscale('log')
            self.plt.xlim([50, 1760])
            self.plt.ylim([-60, 0])
            self.plt.xlabel('Frequency (Hz)')
            self.plt.ylabel('Amplitude (dBFS)')

        hit_targets = set()
        def filter_fn(i):
            freq = to_freq(i)
            if freq < self.LOW_CUT:
                return False
            target = _find_nearest(self.TARGET_FREQS, freq)
            if abs(_to_cents(target, freq)) < self.FREQ_MARGIN_CENTS:
                if target in hit_targets:
                    return False
                hit_targets.add(target)
                if target not in self.TARGET_FUNDAMENTALS:
                    return False
                return True
            else:
                return False

        def improve_guess(i):
            try:
                new_i = parabolic(np.log(f), i)[0]
                if new_i < 0:
                    return None
                return new_i
            except Exception:
                return None

        # Find the peaks and interpolate to get a more accurate peak
        sorted_bins = sorted(range(len(f)), key=lambda i: f[i], reverse=True)
        sorted_f_d = np.array([f_d[i] for i in sorted_bins])
        max_a = np.max(sorted_f_d)
        cutoff = max_a - self.INTENSITY_THRESH
        cutoff_i = np.searchsorted(-sorted_f_d, -cutoff)
        i_s = list(filter(filter_fn, sorted_bins[:cutoff_i]))
        true_i_s = list(filter(lambda x: x is not None, map(improve_guess, i_s)))

        if self.PLOT:
            self.plt.plot([0, 20000], [cutoff]*2)
            for i in true_i_s:
                self.plt.plot([to_freq(i)]*2, [0, -1000], 'r--')
            for f in self.TARGET_FREQS:
                self.plt.plot([f]*2, [0, -1000], 'k:')
            for f in self.TARGET_FUNDAMENTALS:
                self.plt.plot([f]*2, [0, -1000], 'g:')

        if self.PLOT:
            self.plt.pause(0.1)
            self.plt.clf()

        # Convert to equivalent frequency
        return np.array([to_freq(true_i) for true_i in true_i_s])

    class ComputeThread(threading.Thread):
        def __init__(self, parent):
            super().__init__()
            self.parent = parent

        def run(self):
            if self.parent.PLOT:
                self.parent.plt.ion()
            while self.parent.running:
                tstart_real = time.time()
                data = self.parent.data.get()
                if len(data) == 0:
                    break
                dmat = np.frombuffer(data, dtype='<i2')
                tstart = time.time()

                # print(frequency_estimator.freq_from_fft(dmat, self.parent.SAMPLERT)*2) # because magic
                # print(frequency_estimator.freqs_from_fft(dmat, self.parent.SAMPLERT, 5)*2) # because magic
                freqs = np.sort(self.parent.freqs_from_fft(dmat, self.parent.SAMPLERT))
                # print(freqs)
                cents = [_to_cents(_find_nearest(self.parent.TARGET_FUNDAMENTALS, freq), freq) for freq in freqs]
                # print(sorted(zip(cents, freqs), key=lambda v: v[1]))
                print(sorted(zip(enumerate(cents), [self.parent.TARGET_NOTE_NAMES[_find_nearest_i(self.parent.TARGET_FUNDAMENTALS, freq)] for freq in freqs]), key=lambda v: v[0][0]))
                # tunings = ['-' if abs(c) < self.parent.IN_TUNE_THRESH else 'v' if c > 0 else '^' for c in cents]
                # print(tunings)
                tunings = ['x']*len(self.parent.TARGET_FUNDAMENTALS)
                write_values = [float('nan')] * len(self.parent.TARGET_FUNDAMENTALS)
                for c, note_i in zip(cents, [_find_nearest_i(self.parent.TARGET_FUNDAMENTALS, freq) for freq in freqs]):
                    tunings[note_i] = '-' if abs(c) < self.parent.IN_TUNE_THRESH else 'v' if c > 0 else '^'
                    write_values[note_i] = c
                self.parent.writer(write_values)
                print(tunings)
                print(write_values)

                now = time.time()
                print(f'{now - tstart}s user {tstart - tstart_real} io {now - tstart_real}s real')
                self.parent.data.task_done()


    def run(self):
        self.running = True
        dt = self.ComputeThread(self)
        dt.start()
        try:
            cnt = 0
            while True:
                length, data = self.iface.read()
                if length != 0:
                    self._data += data
                    cnt += 1
                    if cnt >= self.BUF_SZ:
                        # clear queue
                        try:
                            while True:
                                self.data.get(block=False)
                                self.data.task_done()
                        except queue.Empty:
                            pass

                        self.data.put(self._data)
                        cnt = 0
                        self._data = b''
        except Exception:
            pass
        except KeyboardInterrupt:
            pass
        self.data.put([])
        self.running = False
        dt.join()


if __name__ == '__main__':
    tuner = Tuner(lambda _:None)
    tuner.run()
