#!/usr/bin/env python3
import tuner
import sys
import os
import struct

def run(pipe=None):
    with open(pipe, 'wb') as p:
        def print_out(data):
            p.write(struct.pack('6f', *data))
            p.flush()

        t = tuner.Tuner(card='default:1', writer=print_out)
        t.run()

if __name__ == '__main__':
    run(sys.argv[1])
